// 1.
// Коли користувач змінює значення то кожен раз подія INPUT запускаэться, 
// на відміну від подій клавіатури, навіть тих, 
// які не передбачають дії клавіатури: вставлення тексту за допомогою миші.



"use strict";
const buttons = document.querySelectorAll(".btn");
const buttonsArray = [...buttons];


buttonsArray.forEach((element) => {
    document.addEventListener("keydown", (e) => {
        if (
            e.code === "Key" + element.innerHTML || 
        e.code === element.innerHTML
        ){
            element.style.background = "blue" ;
        }
        else if (element.style.background === "blue"){
            element.style.background = "black";
        }
    })
})